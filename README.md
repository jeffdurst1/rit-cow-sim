# RIT-cow-sim

Basic Model: Traditional Code for a simple agent-based model of cows grazing in a field

The python code is a simple example of using python to create an agent-based model of cows grazing in a field. The goal is to show how an agent-based model can be created using some simple python code. The problem under study is the grazing behavior of cows in a field of grass. We want to observe how the cows move over time as a function of the availbe grass for grazing. At each model time step, each cow searches for the tallest grass near it, moves to that patch of grass, and eats. Eaten grass also regrows at each time step. Setup is done using an input .json file.

cow-sim-dev: Agent-based model in Godot

This Godot project is the same agent-based model of grazing cows, now implemented in the Godot gaming engine. The main scene file is the grass field, and the cow and grass entities are each modeled as a Godot agent. Using the gaming engine lets us skip over the more complicated aspects of agent-based modeling, such as the internal architecture and message passing. To run, open the cow-sim-dev folder inside Godot.
